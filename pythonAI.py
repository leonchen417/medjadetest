
inputname = input('WHAT IS YOUR name?')

inputfirstproblem = input('Hey '+ inputname +' tell me what problem you have?' )



def get_diag(inputproblem):
    alldiag = {'hypten':'htpertension','diab':'diabetes','hyplip':'hyperlipid','unclear':'unclear'}
    currdiag = alldiag.popitem()
    return currdiag

def get_next_step(currdiag):
	if str(currdiag[0]) == 'unclear':
		text = 'Let me ask you a few more question.'
		nextstep = 'morediag'
	else:
		text = 'Should I make an appointment for you?'
		nextstep ='appointment'
	return text, nextstep

currdiag = get_diag(inputfirstproblem)

#print(str(currdiag[0])=='show')
text,nextstep = get_next_step(currdiag)

print('I think your issue is '+ currdiag[1] +'. '+ text)

